﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class Unit : MonoBehaviour
{
    public float health = 50f;
    public float damage = 5f;
    public float attackSpeed = 1f;
    public float attackRange = 1f;
    public bool projectileAttack = false;
    public Action unitDied;
    public NavMeshAgent agent;

    protected float lastAttackTime;
    protected float originalChildY;
    protected float maxHealth;

    public new AudioSource audio;

    public virtual void Start()
    {
        if (!agent)
            agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = attackRange;
        agent.updateRotation = false;
        originalChildY = transform.GetChild(0).localPosition.y;
        maxHealth = health;
        audio = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        audio.spatialBlend = 0.8f;
    }

    public void ResetHealth()
    {
        health = maxHealth;
    }

    public void SetAgentActive(bool value)
    {
        if (!agent)
            agent = GetComponent<NavMeshAgent>();
        agent.enabled = value;
    }

    public virtual void Attack(Unit target, float damage)
    {
        target.TakeDamage(damage);
        Debug.Log(name + " Attacked " + target + " damage " + damage);
    }

    public void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Debug.Log(name + " Dead! ");
            unitDied?.Invoke();
        }
        else
        {
            DamagedEffect();
            audio.PlayOneShot(Manager.instance.damageTaken);
        }
    }

    public virtual void Update()
    {
        if (agent && agent.enabled)
        {
            var pos = transform.GetChild(0).transform.localPosition;
            pos.y = originalChildY + Mathf.Sin(Time.time * Mathf.Clamp(agent.velocity.sqrMagnitude, 0, 2f) * 2f) * agent.velocity.sqrMagnitude * 0.03f;
            transform.GetChild(0).transform.localPosition = pos;
        }
        else
        {
            if (transform.GetChild(0) && transform.GetChild(0).localPosition.y != originalChildY)
            {
                var pos = transform.GetChild(0).transform.localPosition;
                pos.y = originalChildY;
                transform.GetChild(0).transform.localPosition = pos;
            }
        }
    }

    public void RotateChildren(Quaternion rotation)
    {
        /*
        foreach (Transform child in transform)
        {
            child.rotation = rotation;
        }
        */
        //transform.rotation = rotation;
    }

    public void DamagedEffect()
    {
        StartCoroutine(DamagedCoroutine());
    }

    IEnumerator DamagedCoroutine()
    {
        float time = 0.3f;

        var sprites = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sprite in sprites)
        {
            sprite.color = Color.red;
        }

        while (time > 0)
        {
            time -= Time.deltaTime;
            transform.position += UnityEngine.Random.insideUnitSphere * 0.07f;
            yield return null;
        }

        foreach (var sprite in sprites)
        {
            sprite.color = Color.white;
        }
    }

}
