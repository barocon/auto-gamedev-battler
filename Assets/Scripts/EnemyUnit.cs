﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyUnit : Unit
{
    public PlayerUnit targetPlayer;
    public override void Start()
    {
        base.Start();
        RotateChildren(Camera.main.transform.rotation);
        unitDied += DestroyOnDeath;
    }

    void DestroyOnDeath()
    {
        Manager.instance.enemies.Remove(this);
        Manager.instance.fxSource.PlayOneShot(Manager.instance.spideDies);
        Destroy(gameObject);
    }

    public override void Update()
    {
        base.Update();

        if (agent && agent.enabled)
        {
            if (targetPlayer == null)
            {
                targetPlayer = FindClosestPlayer();
                audio.PlayOneShot(Manager.instance.spiderMove);
            }
            else
            {
                agent.SetDestination(targetPlayer.transform.position);
                if (agent.pathStatus == NavMeshPathStatus.PathComplete)
                {
                    if (Vector3.Distance(transform.position, targetPlayer.transform.position) < attackRange)
                    {
                        if (Time.time > lastAttackTime + attackSpeed)
                        {
                            lastAttackTime = Time.time;
                            Attack(targetPlayer, damage);
                        }
                    }
                }
                if (targetPlayer.health <= 0)
                    targetPlayer = null;
            }
        }
    }

    PlayerUnit FindClosestPlayer()
    {
        if (Manager.instance.unitsInArena != null && Manager.instance.unitsInArena.Count > 0)
        {
            PlayerUnit closest = null;
            float closestDist = Mathf.Infinity;
            foreach (var enemy in Manager.instance.unitsInArena)
            {
                if (!enemy)
                    continue;
                if (enemy.health <= 0)
                    continue;

                var dist = Vector3.Distance(transform.position, enemy.transform.position);
                if (dist <= closestDist)
                {
                    closest = enemy;
                    closestDist = dist;
                }
            }
            return closest;
        }
        return null;
    }
}
