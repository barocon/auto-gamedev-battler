﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public static Manager instance;

    public const int tier1Price = 1;
    public const int tier2Price = 3;
    public const int tier3Price = 5;

    public GameObject shopCamera;
    public TextMeshPro textPrefab;
    public Transform benchMiddle;
    public Transform enemyMiddle;
    public Button shopButton;
    public TextMeshProUGUI goldTextUI;
    public GameObject selectionIndicator;

    public TierSettings[] tierSettings;
    public RoundSettings[] roundSettings;
    public PlayerLevelSettings[] playerLevelSettings;
    public List<PlayerUnit> playerUnitBench = new List<PlayerUnit>();
    public List<PlayerUnit> unitsInShop = new List<PlayerUnit>();
    public List<PlayerUnit> unitsInArena = new List<PlayerUnit>();
    public List<GameObject> shopUI = new List<GameObject>();
    public List<EnemyUnit> enemies = new List<EnemyUnit>();
    public PlayerUnit selectedUnit;
    public Dictionary<string, int> arenaUnitCount = new Dictionary<string, int>();

    public AudioClip battleMusic, titleMusic;
    public AudioClip damageTaken, playerDies, playerSelect, spiderMove, spideDies;
    public AudioSource musicSource;
    public AudioSource fxSource;

    public GameObject youWinUI;


    int playerGold = 2;
    int playerLevel = 1;
    int roundNumber = 0;
    float roundTimer;
    bool shopActiveStatus;
    bool combatActive;

    public int brooding, buzzing, joker;
    public int coder, artist, manager;

    [System.Serializable]
    public class TierSettings
    {
        public PlayerUnit[] playerUnits;
    }
    [System.Serializable]
    public class RoundSettings
    {
        public float roundLength = 100f;
        public EnemyUnit[] enemyUnits;
    }
    [System.Serializable]
    public class PlayerLevelSettings
    {
        public int tier1Shoprate = 80;
        public int tier2Shoprate = 20;
        public int tier3Shoprate = 5;
    }

    private void Awake()
    {
        instance = this;
        SpawnLevelShop();
        UpdateGoldUI();
        SpawnRoundEnemies(roundNumber);
        youWinUI.SetActive(false);
    }

    public void PlayTitle()
    {
        musicSource.clip = titleMusic;
        musicSource.Play();
    }

    public void PlayBattle()
    {
        musicSource.clip = battleMusic;
        musicSource.Play();
    }

    public void SpawnLevelShop()
    {
        PlayTitle();

        // clear old shop
        foreach (var old in unitsInShop)
        {
            if (old)
                Destroy(old.gameObject);
        }
        unitsInShop.Clear();

        // clear old text
        foreach (var text in shopUI)
        {
            if (text)
                Destroy(text);
        }
        shopUI.Clear();

        var weights = new Dictionary<PlayerUnit, int>();

        foreach (var unit in tierSettings[0].playerUnits)
        {
            weights.Add(unit, playerLevelSettings[playerLevel].tier1Shoprate);
        }
        foreach (var unit in tierSettings[1].playerUnits)
        {
            weights.Add(unit, playerLevelSettings[playerLevel].tier2Shoprate);
        }
        foreach (var unit in tierSettings[2].playerUnits)
        {
            weights.Add(unit, playerLevelSettings[playerLevel].tier3Shoprate);
        }

        Transform st = shopCamera.transform;
        Vector3 middlePos = st.position + st.forward * 2f;
        Vector3 posLeft = middlePos + st.right * -11f;
        Vector3 posRight = middlePos + st.right * 11f;

        var unitRotation = st.rotation;
        for (int i = 0; i < 5; i++)
        {
            var selected = WeightedRandomizer.From(weights).TakeOne();
            float t = (float)i / 4f;
            var pos = Vector3.Lerp(posLeft, posRight, t);
            //Debug.Log(selected.name + " pos " + pos + " t " + t);
            var newShopUnit = Instantiate(selected, pos, unitRotation, st);
            newShopUnit.name = selected.name;
            unitsInShop.Add(newShopUnit);
            newShopUnit.SetAgentActive(false);
        }

        foreach (var shopUnit in unitsInShop)
        {
            var newText = Instantiate(textPrefab, shopUnit.transform.position - st.forward, st.rotation, st);
            if (!newText)
                Debug.LogError("Missing text");
            if (!shopUnit)
                Debug.LogError("Missing shopUnit");
            shopUI.Add(newText.gameObject);
            newText.text = shopUnit.name + "\n" + 
                shopUnit.personality.name + " " + shopUnit.role.name + "\n" +
                + GetTierPrice(shopUnit.tier) + "g";

            var newButton = Instantiate(shopButton, shopUnit.transform.position + st.forward, st.rotation, st);
            newButton.onClick.AddListener(delegate { Purchase(shopUnit); } );
            shopUI.Add(newButton.gameObject);
            shopUnit.uishit.Add(newButton.gameObject);
            shopUnit.uishit.Add(newText.gameObject);
        }

        SetShopActive(true);
    }

    public int GetTierPrice(int tier)
    {
        if (tier == 1)
            return tier1Price;
        if (tier == 2)
            return tier2Price;
        if (tier == 3)
            return tier3Price;
        return 0;
    }

    public void SpawnRoundEnemies(int level)
    {
        if (enemies != null && enemies.Count > 0)
        {
            foreach (var en in enemies)
            {
                if (en)
                    Destroy(en.gameObject);
            }
            enemies.Clear();
        }


        var enemyLeft = enemyMiddle.position - enemyMiddle.right * 8f;
        var enemyRight = enemyMiddle.position + enemyMiddle.right * 8f;

        int index = 0;
        foreach (var enemy in roundSettings[level].enemyUnits)
        {
            float t = (float)index / (float)(roundSettings[level].enemyUnits.Length);
            var newEnemy = Instantiate(enemy, Vector3.Lerp(enemyLeft, enemyRight, t), Quaternion.identity, enemyMiddle);
            enemies.Add(newEnemy);
            newEnemy.SetAgentActive(false);
            //Debug.Log(newEnemy.name + " t " + t);
            index++;
        }
    }

    public void ToggleShopActive()
    {
        SetShopActive(!shopActiveStatus);
    }

    public void SetShopActive(bool value)
    {
        shopActiveStatus = value;
        shopCamera.SetActive(value);


    }
    public void Purchase(PlayerUnit playerUnit)
    {
        var price = GetTierPrice(playerUnit.tier);
        if (playerGold >= price && CanBenchUnits())
        {
            playerGold -= price;
            unitsInShop.Remove(playerUnit);
            MoveUnitToBench(playerUnit);
            UpdateGoldUI();
            foreach (var item in playerUnit.uishit)
            {
                Destroy(item);
            }
            playerUnit.uishit.Clear();
            fxSource.PlayOneShot(playerSelect);
        }
        else
        {
            Debug.Log("Not enough money!");
        }
    }

    public void UpdateGoldUI()
    {
        goldTextUI.text = playerGold + " GOLD";
    }

    public void DestroyObject(GameObject obj)
    {
        Destroy(obj);
    }

    public bool CanBenchUnits()
    {
        if (playerUnitBench.Count < 8)
            return true;
        return false;
    }

    void MoveUnitToBench(PlayerUnit unit)
    {
        if (!CanBenchUnits())
            return;
        playerUnitBench.Add(unit);
        ResetBenchPosition();
        unit.SetAgentActive(false);
        unit.transform.parent = benchMiddle;
        unit.RotateChildren(Camera.main.transform.rotation);
    }

    void MoveSelectedUnitInArena(PlayerUnit unit, Vector3 position)
    {
        if (playerUnitBench.Contains(unit))
        {
            playerUnitBench.Remove(unit);
            if (!arenaUnitCount.ContainsKey(unit.name))
                arenaUnitCount.Add(unit.name, 1);
            else
                arenaUnitCount[unit.name]++;

        }
        if (!unitsInArena.Contains(unit))
            unitsInArena.Add(unit);

        ResetBenchPosition();
        unit.SetAgentActive(false);
        unit.transform.position = position;
        selectedUnit = null;
        selectionIndicator.SetActive(false);
        unit.RotateChildren(Camera.main.transform.rotation);
        UpdateBonuses();

        if (arenaUnitCount[unit.name] >= 3)
        {
            PlayerUnit unit1 = null; PlayerUnit unit2 = null; PlayerUnit unit3 = null;

            foreach (var item in unitsInArena)
            {
                if (unit1 == null && item.name == unit.name)
                {
                    unit1 = item;
                    continue;
                }
                if (unit2 == null && item.name == unit.name)
                {
                    unit2 = item;
                    continue;
                }
                if (unit3 == null && item.name == unit.name)
                {
                    unit3 = item;
                    UpgradeUnit(unit1, unit2, unit3);
                    break;
                }
            }
        }
    }

    void UpgradeUnit(PlayerUnit unit1, PlayerUnit unit2, PlayerUnit unit3)
    {
        var unit = unit2;
        if (playerUnitBench.Contains(unit))
            playerUnitBench.Remove(unit);
        if (unitsInArena.Contains(unit))
        {
            unitsInArena.Remove(unit);
            arenaUnitCount[unit.name]--;
        }
        Destroy(unit.gameObject);

        unit = unit3;
        if (playerUnitBench.Contains(unit))
            playerUnitBench.Remove(unit);
        if (unitsInArena.Contains(unit))
        {
            unitsInArena.Remove(unit);
            arenaUnitCount[unit.name]--;
        }
        Destroy(unit.gameObject);

        fxSource.PlayOneShot(playerSelect);

        UpdateBonuses();
        unit1.Upgrade();
    }

    public void SellUnit()
    {
        if (selectedUnit)
        {
            var unit = selectedUnit;
            if (playerUnitBench.Contains(unit))
                playerUnitBench.Remove(unit);
            if (unitsInArena.Contains(unit))
            {
                unitsInArena.Remove(unit);
                arenaUnitCount[unit.name]--;
            }
            playerGold += GetTierPrice(unit.tier);
            UpdateGoldUI();
            selectedUnit = null;
            selectionIndicator.SetActive(false);
            Destroy(unit.gameObject);
            UpdateBonuses();
        }
    }

    void UpdateBonuses()
    {
        brooding = 0; buzzing = 0; joker = 0;
        coder = 0; artist = 0; manager = 0;

        foreach (var unit in unitsInArena)
        {
            if (unit.role.name == "Brooding")
                brooding++;
            if (unit.role.name == "Buzzing")
                buzzing++;
            if (unit.role.name == "Joker")
                joker++;
            if (unit.role.name == "Coder")
                coder++;
            if (unit.role.name == "Artist")
                artist++;
            if (unit.role.name == "Manager")
                manager++;
        }
    }

    public void SelectUnit(PlayerUnit unit)
    {
        if (playerUnitBench.Contains(unit) || unitsInArena.Contains(unit))
        {
            selectedUnit = unit;
            selectionIndicator.transform.position = unit.transform.position;
            selectionIndicator.SetActive(true);
            fxSource.PlayOneShot(playerSelect);
        }
    }

    private void Update()
    {
        if (combatActive && enemies.Count == 0)
        {
            combatActive = false;
            EndRound(true);
        }
        if (combatActive && unitsInArena.Count > 0)
        {
            bool alldead = true;
            foreach (var item in unitsInArena)
            {
                if (item.gameObject.activeSelf)
                {
                    alldead = false;
                }
            }
            if (alldead)
            {
                combatActive = false;
                EndRound(false);
            }

        }

        if (selectedUnit != null && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            // Casts the ray and get the first game object hit
            Physics.Raycast(ray, out hit);
            if (hit.collider.CompareTag("Ground"))
            {
                Debug.Log("This hit at " + hit.point);
                MoveSelectedUnitInArena(selectedUnit, hit.point);
                selectedUnit = null;
            }
        }
    }

    public void EndRound(bool won)
    {
        foreach (var unit in unitsInArena)
        {
            unit.transform.position = unit.arenaStartPosition;
            unit.gameObject.SetActive(true);
            unit.SetAgentActive(false);
            unit.ResetHealth();
        }
        roundNumber++;
        if (won)
        {
            if (roundNumber == 9)
            {
                youWinUI.SetActive(true);
                return;
            }

            playerLevel++;
            AddGold(roundNumber * 3);
        }
        SpawnRoundEnemies(roundNumber);
        SpawnLevelShop();
    }

    public void AddGold(int gold)
    {
        playerGold += gold;
        UpdateGoldUI();
    }

    void ResetBenchPosition()
    {
        var benchLeft = benchMiddle.position - benchMiddle.right * 8f;
        var benchRight = benchMiddle.position + benchMiddle.right * 8f;

        int index = 0;
        foreach (var unit in playerUnitBench)
        {
            index++;
            float t = (float)(index / 8f);
            unit.transform.position = Vector3.Lerp(benchLeft, benchRight, t);
        }
    }

    public void StartCombat()
    {
        if (combatActive)
            return;

        PlayBattle();

        SetShopActive(false);

        combatActive = true;
        foreach (var unit in unitsInArena)
        {
            if (unit)
            {
                unit.arenaStartPosition = unit.transform.position;
                unit.SetAgentActive(true);
            }

        }
        foreach (var unit in enemies)
        {
            unit.SetAgentActive(true);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
