﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "personality", menuName = "Personality", order = 1)]
public class PersonalityScriptableObject : ScriptableObject
{
    public string description = "new personality";
    [SerializeField] Personality personality;
    public GameObject headMesh;
    public Material headMaterial;

    public enum Personality { Buzzing, Brooding, Chatty, Joker };
}
