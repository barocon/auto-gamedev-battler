﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "role", menuName = "Role", order = 1)]
public class RoleScriptableObject : ScriptableObject
{
    public string description = "new Role";
    [SerializeField] Role role;
    public GameObject torsoMesh;
    public Material torsoMaterial;

    public enum Role { Coder, Artist, Marketing, Manager, Sound, QA, Designer };
}
