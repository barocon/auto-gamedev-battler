﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerUnit : Unit
{
    public RoleScriptableObject role;
    public PersonalityScriptableObject personality;
    public List<GameObject> uishit = new List<GameObject>();
    public EnemyUnit targetEnemy;
    public Vector3 arenaStartPosition;

    [Range(1, 3)]
    public int tier = 1;

    private void Awake()
    {
        Instantiate(personality.headMesh, transform);
        Instantiate(role.torsoMesh, transform);
        unitDied += DisableOnDeath;
    }

    void DisableOnDeath()
    {
        gameObject.SetActive(false);
        Manager.instance.fxSource.PlayOneShot(Manager.instance.playerDies);
    }

    public void Upgrade()
    {
        damage *= 3f;
        health *= 3f;
        maxHealth *= 3f;
        agent.speed *= 1.5f;
        agent.acceleration *= 1.5f;
        attackRange *= 1.2f;
        agent.stoppingDistance *= 1.2f;
        transform.localScale *= 1.2f;
    }

    public override void Attack(Unit target, float damage)
    {
        /*
        if (role.name == "Brooding" && Manager.instance.brooding >= 2)

        if (role.name == "Buzzing")

        if (role.name == "Joker")

        if (role.name == "Coder")

        if (role.name == "Artist")

        if (role.name == "Manager")
        */

        base.Attack(target, damage);
    }

    public override void Update()
    {
        base.Update();

        if (agent && agent.enabled)
        {
            if (targetEnemy == null)
            {
                targetEnemy = FindClosestEnemy();
            }
            else
            {
                agent.SetDestination(targetEnemy.transform.position);
                if (agent.pathStatus == NavMeshPathStatus.PathComplete)
                {
                    if (Vector3.Distance(transform.position, targetEnemy.transform.position) < attackRange)
                    {
                        if (Time.time > lastAttackTime + attackSpeed)
                        {
                            lastAttackTime = Time.time;
                            Attack(targetEnemy, damage);
                        }
                    }
                }
            }
        }
    }

    EnemyUnit FindClosestEnemy()
    {
        if (Manager.instance.enemies != null && Manager.instance.enemies.Count > 0)
        {
            EnemyUnit closest = null;
            float closestDist = Mathf.Infinity;
            foreach (var enemy in Manager.instance.enemies)
            {
                if (!enemy)
                    continue;

                var dist = Vector3.Distance(transform.position, enemy.transform.position);
                if (dist <= closestDist)
                {
                    closest = enemy;
                    closestDist = dist;
                }
            }
            return closest;
        }
        return null;
    }

    public void OnMouseDown()
    {
        Manager.instance.SelectUnit(this);
        Debug.Log(name + "  OnMouseDown");
    }
}
